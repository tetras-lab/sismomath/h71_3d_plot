# nonlinloc_3D_plot

This takes a list of `.hypo_71` or `.hyp` (hypocenter phase file") formated files, generated by a nonlinloc process, and displays the infered epicenters of earthquaques in a 3D scatter graph.

Feature roadmap is :

- Filter by date and magnitude
- Show uncertainty elipsoids
- Show the corresponding points for different nonlinloc executions when a point is clicked on the graph.