from sys import exit

from dash import dcc, html, Input, Output, State, callback, callback_context, clientside_callback
import dash_bootstrap_components as dbc

import plotly.express as px
from plotly.figure_factory import create_distplot
import plotly.graph_objs as go
from plotly import colors

import json
import pandas as pd
from obspy import read_events
from datetime import date, datetime
from dash_datetimepicker import DashDatetimepicker
from datetime import datetime 

def create_fig(df, mask='', opacity=0.8):
    if str(mask)=='':
        df2=df
    else:
        df2=df[mask]
    fig = px.scatter_3d(df2, x='LONG_DD', y='LAT_DD', z='DEPTH_N', size='MAG', custom_data='DATETIME', opacity=opacity, color="PARAMETERS SET", width=900, height=680)
    fig.update_layout(clickmode='event+select', margin_t=40, margin_b=20, margin_l=5, margin_r=5, legend=dict(title='Filter by parameter set:'))#, yanchor="top", xanchor="right")) #modebar={'orientation': 'v'}
    fig.update_traces(hovertemplate= "LONG_DD=%{x}<br>LAT_DD=%{y}<br>DEPTH_N=%{z}<br>MAG=%{marker.size}<br>DATE=%{customdata}<extra></extra>")
    return(fig)

# Function that search/replace a string in a file 
def replace_in_file(filePath, searchStr, replaceStr):
    # Read in the file
    with open(filePath, 'r') as file:
      filedata = file.read()
    # Replace the target string
    filedata = filedata.replace(searchStr, replaceStr)
    # Write the file out again
    with open(filePath, 'w') as file:
      file.write(filedata)

# Function to convert a Degree Decimal Minutes str array to a Decimal Degree float array
def ddm_to_dd(array):
    tmpDf = array.str.replace('  ',' ').str.split(' ', expand=True)
    return(tmpDf[0].astype(int) + tmpDf[1].astype(float)/60)

# DEPRECATED : Function to read a h71 file list and prepare a locDf dataframe for 3D plot (one color for each file)
def import_hypo_71_list(inputFileList):
    dfList = []
    for inputFile in inputFileList:
        if not(inputFile.endswith('.hypo_71')):
            print("File {} is not in the .hypo_71 format".format(inputFile))
            exit(1)
        with open(inputFile) as f:
            paramName = f.readline().strip('\n')
        locDfTMP = pd.read_fwf(inputFile,skiprows=1)
        locDfTMP.MAG = locDfTMP.MAG.astype(float)
        locDfTMP['DEPTH_N'] = -locDfTMP.DEPTH.astype(float)
        locDfTMP['LONG_DD'] = ddm_to_dd(locDfTMP['LONG'])
        locDfTMP['LAT_DD'] = ddm_to_dd(locDfTMP['LAT'])
        locDfTMP['PARAMETERS SET'] = paramName 
        dfList.append(locDfTMP)
    locDf = pd.concat(dfList, ignore_index=True)
    #pd.to_datetime(locDf.DATE, format='%d%m%y')
    return(locDf)

def import_hyp_list(inputFileList):
    dfList = []
    catList = []
    for i in range(len(inputFileList)):
        inputFile = inputFileList[i]
        # The following replace QUALITY with COMMENT in the input file because the QUALITY line is ignored by obspy.read_events .
        # We need the QUALITY line because it contains the Mamp attribute that is the magnitude.
        # As a trivial workaround, we make this replacement so the QUALITY line is stored as a comment that we can parse to get Mamp
        # BEWARE : the QAULITY line will override the original COMMENT line that is present for an event in the file
        # This workaround should be replaced by something cleaner in the future (it also has implications when getting the Mamp value bellow).
        replace_in_file(inputFile, "QUALITY ", "COMMENT ")
        if not(inputFile.endswith('.hyp')):
            print("File {} is not in the .hyp format (hypocenter phase file)".format(inputFile))
            exit(1)
        # Load the event catalog using obspy.read_events
        cat = read_events(inputFile)
        for event in cat:
            tmpDf = pd.DataFrame(event.origins)
            tmpDf['PARAMETERS SET'] = inputFile.split('/')[-1]
            tmpDf['IMPORT_NUM'] = i
            tmpDf['depth_uncertainty'] = event.origins[0].depth_errors.uncertainty
            tmpDf['long_uncertainty'] = event.origins[0].longitude_errors.uncertainty
            tmpDf['lat_uncertainty'] = event.origins[0].latitude_errors.uncertainty
            # The following works because of the QUELITY/COMMENT workaround (see above)
            qualityList = event.comments[0].text.replace('  ',' ').split(' ')
            MampIndex = qualityList.index('Mamp') + 1
            tmpDf['MAG'] = float(qualityList[MampIndex])
            dfList.append(tmpDf)
    locDf = pd.concat(dfList, ignore_index=True)
    locDf.rename(columns={"latitude": "LAT_DD", "longitude": "LONG_DD", "time": "DATETIME"}, inplace=True)
    locDf['DEPTH_N'] = -locDf['depth']
    maxMag = locDf.MAG.max()
    maxDatetime = locDf.DATETIME.max()
    minDatetime = locDf.DATETIME.min()
    maxLAT_DD = locDf.LAT_DD.max()
    minLAT_DD = locDf.LAT_DD.min()
    maxLONG_DD = locDf.LONG_DD.max()
    minLONG_DD = locDf.LONG_DD.min()
    maxDEPTH_N = locDf.DEPTH_N.max()
    minDEPTH_N = locDf.DEPTH_N.min()
    return (locDf, maxMag, minDatetime, maxDatetime, minLAT_DD, maxLAT_DD, minLONG_DD, maxLONG_DD, minDEPTH_N, maxDEPTH_N)

def create_layout(locDf, maxMag, minDatetime, maxDatetime, minLAT_DD, maxLAT_DD, minLONG_DD, maxLONG_DD, minDEPTH_N, maxDEPTH_N):
    distplot = create_distplot([locDf.MAG], ['MAG'],bin_size=maxMag/50, show_rug=False, show_curve=False)
    # The following trick allows the xaxis to start at 0 by adding a transparent line to the plot
    distplot.add_vline(x=0, line_color="rgba(0,0,0,0)")
    update = distplot.update_layout(showlegend=False, height=60, margin_t=0, margin_b=0, margin_l=20, margin_r=20), 
    update = distplot.update_yaxes(visible=False)
    update = distplot.update_xaxes(visible=False)
    fig = create_fig(locDf)
    layout = html.Div([
    dbc.Col([
        # To add a loading indicator, uncomment the following line and comment the one after + comment the computation of the first version of the graph above
        #dcc.Loading(dcc.Graph(id="scatter3D")),
        html.Div(id='selected_points', style={'display': 'none'}),
        #html.Div(id='selected_points'),
        dcc.Store(id='clientside-figure-store', data= fig),#json(locDf.to_dict('records')) )
       # html.Div(fig.to_json()),
        dbc.Row([
            dbc.Col(dcc.Graph(id="scatter3D", figure=fig, config = {'displayModeBar': True})),
            dbc.Col([
                html.Button('Clear Selection', id='clear'),
                html.P(),
                html.P("Filter by magnitude:"),
                dcc.Graph(id="distplot", figure=distplot, config = {'staticPlot': True}),
                dcc.RangeSlider(id='magnitude-range-slider', 
                                min=0, 
                                max=maxMag, 
                                allowCross=False, 
                                tooltip={"placement": "bottom", "always_visible": True},
                                value=[0, maxMag]
                               ),
                html.P("Filter by coordinates:"),
                html.P("  Latitude:"),
                dcc.RangeSlider(id='lat-range-slider', 
                                min=minLAT_DD, 
                                max=maxLAT_DD, 
                                allowCross=False, 
                                tooltip={"placement": "bottom", "always_visible": True},
                                marks=None,
                                value=[minLAT_DD, maxLAT_DD]
                               ),
                html.P("  Longitude:"),
                dcc.RangeSlider(id='long-range-slider', 
                                min=minLONG_DD, 
                                max=maxLONG_DD, 
                                allowCross=False, 
                                tooltip={"placement": "bottom", "always_visible": True},
                                marks=None,
                                value=[minLONG_DD, maxLONG_DD]
                               ),
                html.P("  Depth:"),
                dcc.RangeSlider(id='depth-range-slider', 
                                min=minDEPTH_N, 
                                max=maxDEPTH_N, 
                                allowCross=False, 
                                tooltip={"placement": "bottom", "always_visible": True},
                                marks=None,
                                value=[minDEPTH_N, maxDEPTH_N]
                               ),
                html.P(),
                html.P("Filter by date and time:",style={'margin-top': '7px'}),
                #dcc.DatePickerRange(
                dbc.Row(DashDatetimepicker(
                                    id='date-picker-range',
                                    #min_date_allowed=date(minDatetime.datetime.year, minDatetime.datetime.month, minDatetime.datetime.day),
                                    #max_date_allowed=date(maxDatetime.datetime.year, maxDatetime.datetime.month, maxDatetime.datetime.day),
                                    #start-date=minDatetime,
                                    #end-date=maxDatetime,
                                    startDate=minDatetime,
                                    endDate=maxDatetime,
                                    #display_format='DD/MM/YY',
                                   ), style={'margin-right': '10px', 'margin-left': '10px'}),
                #dbc.Row([dcc.Input(id='time-start',type='time'),dcc.Input(id='time-stop',type='time')]),
            ], width=4),
        ]),
    ])
])
    return layout

def create_dummy_layout():
    now = datetime.now() 
    locDf = pd.DataFrame([[0,0,0,1,now,'']], columns=["LAT_DD", "LONG_DD", "DEPTH_N", 'MAG','DATETIME','PARAMETERS SET']) 
    maxMag = 6 
    minDatetime = now
    maxDatetime = now 
    minLAT_DD, maxLAT_DD, minLONG_DD, maxLONG_DD, minDEPTH_N, maxDEPTH_N = 0, 0, 0, 0, 0, 0
    layout = create_layout(locDf, maxMag, minDatetime, maxDatetime, minLAT_DD, maxLAT_DD, minLONG_DD, maxLONG_DD, minDEPTH_N, maxDEPTH_N)
    return(layout)

# Callback to store selected points in a hiden div

@callback(Output('selected_points', 'children'),
            [Input('scatter3D', 'clickData'),
                Input('clear', 'n_clicks')],
            [State('selected_points', 'children')])
def select_point(clickData, clear_clicked, selected_points):
    ctx = callback_context
    ids = [c['prop_id'] for c in ctx.triggered]

    if selected_points:
        results = json.loads(selected_points)
    else:
        results = []
    if 'scatter3D.clickData' in ids:
        if clickData:
            for p in clickData['points']:
                if p not in results:
                    results.append(p)
                #else :
                #    results.remove(p)
    if 'clear.n_clicks' in ids:
        results = []
    results = json.dumps(results)
    return results


# Callback to filter the plot (magnitude and datetime)
# and highlight selected points
# Runs clientside

clientside_callback(
    """function processAndFilterJsonData(magnitudeMinMax, latMinMax, longMinMax, depthMinMax, startDate, endDate, selected_points, json) {
    // Filter JSON data based on magnitude and date range
    const magnitudeMin = magnitudeMinMax[0];
    const magnitudeMax = magnitudeMinMax[1];
    const latMin = latMinMax[0];
    const latMax = latMinMax[1];
    const longMin = longMinMax[0];
    const longMax = longMinMax[1];
    const depthMin = depthMinMax[0];
    const depthMax = depthMinMax[1];
    const start = new Date(startDate);
    const end = new Date(endDate);
    
    let filteredJson = {
        ...json,
        data: json.data.map(item => {
            const filteredIndexes = item.marker.size
                .map((value, index) => {
                    const date = new Date(item.customdata[index][0]);
                    const x = item.x[index];
                    const y = item.y[index];
                    const z = item.z[index];
                    return (
                        value >= magnitudeMin &&
                        value <= magnitudeMax &&
                        x >= longMin &&
                        x <= longMax &&
                        y >= latMin &&
                        y <= latMax &&
                        z >= depthMin &&
                        z <= depthMax &&
                        date >= start &&
                        date <= end
                    ) ? index : -1;
                })
                .filter(index => index !== -1);

            return {
                ...item,
                marker: {
                    ...item.marker,
                    size: filteredIndexes.map(index => item.marker.size[index])
                },
                x: filteredIndexes.map(index => item.x[index]),
                y: filteredIndexes.map(index => item.y[index]),
                z: filteredIndexes.map(index => item.z[index]),
                customdata: filteredIndexes.map(index => item.customdata[index])
            };
        })
    };

    // Process selected points and add new traces
    if (!filteredJson) {
        return filteredJson;
    }

    let fig = JSON.parse(JSON.stringify(filteredJson));
    selected_points = selected_points ? JSON.parse(selected_points) : [];
    let colors = [
        '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', 
        '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'
    ];

    let dataSize = fig.data.length;

    if (selected_points.length > 0) {
        let i = 0;
        selected_points.forEach(function(p) {
               let pointNumber = p["pointNumber"];
               let color = colors[i % colors.length];
               i++;
               let x = [];
               let y = [];
               let z = [];
               fig.data.forEach(function(curve) {
                   if (curve.marker.symbol !== 'diamond-open') {
                       x.push(curve.x[pointNumber]);
                       y.push(curve.y[pointNumber]);
                       z.push(curve.z[pointNumber]);
                   }
               });
               let new_trace = {
                   type: 'scatter3d',
                   mode: 'markers',
                   x: x,
                   y: y,
                   z: z,
                   marker: {
                       size: 8,
                       symbol: 'diamond-open',
                       color: color,
                       opacity: 0.8,
                       line: {
                           color: color,
                           width: 3
                       }
                   },
                   hoverinfo: 'skip',
                   showlegend: false
               };
               fig.data.push(new_trace);
        });
    }
    return fig;
}
    """,
    Output("scatter3D", "figure"),
    [
        Input("magnitude-range-slider", "value"),
        Input("lat-range-slider", "value"),
        Input("long-range-slider", "value"),
        Input("depth-range-slider", "value"),
        Input("date-picker-range", "startDate"),
        Input("date-picker-range", "endDate"),
        Input('selected_points', 'children')
    ],
    [
        State('clientside-figure-store', 'data'),
        State("scatter3D", "relayoutData")
    ],
    prevent_initial_call=True
)

